FROM php:7.2-fpm

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    mysql-client \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl
RUN docker-php-ext-configure gd --with-gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-png-dir=/usr/include/
RUN docker-php-ext-install gd

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# composer env to ignore running as root warning
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV PATH="./vendor/bin:$PATH"

# Install hirak/prestissimo for faster composer installs
RUN composer global require hirak/prestissimo --no-plugins --no-scripts

# copy only composer.json & composer.lock to ensure composer doesn't invalidate composer install files on next build if they have not changed
COPY composer.lock /var/www/html/composer.json
COPY composer.lock /var/www/html/composer.lock

# Set working directory & composer install while removing composer cache after installing
WORKDIR /var/www/html
RUN composer install --prefer-dist --no-scripts --no-dev --no-autoloader && rm -rf /root/.composer

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

# Now copy existing application directory content after composer install
COPY . /var/www/html

# Copy existing application directory permissions
RUN chown -R www:www /var/www/html

# Change current user to www
USER www

# Finish composer install creating autoloader and optimizing
RUN composer dump-autoload --no-scripts --no-dev --optimize

# # Ensure that phpfpm doesn't ignore our ENV variables
# RUN sed -e 's/;clear_env = no/clear_env = no/' -i /etc/php/7.2/php-fpm.d/www.conf

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
